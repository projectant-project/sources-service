import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SourcesModule } from './sources/sources.module';

@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost:27017/'), SourcesModule],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
