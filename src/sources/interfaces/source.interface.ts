import { Document } from 'mongoose';

export interface Source extends Document{
    uri: string,
    title: string,
    description: string
}