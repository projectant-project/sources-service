import * as mongoose from "mongoose";

export const SourceSchema = new mongoose.Schema({
    uri: String,
    title: String,
    description: String
});