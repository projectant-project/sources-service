import { InjectModel } from '@nestjs/mongoose';
import { Injectable, BadRequestException, InternalServerErrorException } from '@nestjs/common';
import { Model } from 'mongoose';
import { Source } from './interfaces/source.interface';
import { SourceDTO } from './dto/create.dto';

import { isNullOrUndefined } from 'util';
import { UpdateDTO } from './dto/update.dto';

@Injectable()
export class SourcesService {
    constructor(@InjectModel('Source') private readonly sourceModel: Model<Source>) { }

    async getAllSources(): Promise<Source[]> {
        return await this.sourceModel.find();
    }

    async getSourceByUri(uri: String): Promise<Source> {
        return await this.sourceModel.findOne({ uri });
    }

    async createSource(sourceDto: SourceDTO): Promise<Source> {
        const source: Source = await this.getSourceByUri(sourceDto.uri);

        if (!isNullOrUndefined(source)) {
            console.log('URI already exists');
            throw new BadRequestException('URI already exists');
        }

        const createSource = new this.sourceModel(sourceDto);
        console.log('Source created successfully');
        return createSource.save();
    }

    async deleteSourceByUri(uri: string) {
        try {
            return await this.sourceModel.deleteOne({ uri });
        }
        catch(error) {
            console.log(error);
        }
    }

    async deleteSourceByTitle(title: string) {
        try {
            return await this.sourceModel.deleteOne({ title });
        }
        catch(error) {
            console.log(error);
        }
    }

    // delete by uri
    /*
    async deleteSourceByUri(uri: String) {
        const source: Source = await this.getSourceByUri(uri);

        if (isNullOrUndefined(source)) {
            console.log('source not found');
            throw new BadRequestException('Source not found');
        }

        const deleted = await this.sourceModel.deleteOne({ uri });

        if (deleted.n === 0 || deleted.ok !== 1) {
            console.log('Error Occured');
            throw new InternalServerErrorException('Error Occured while trying to delete');
        }

        console.log(deleted);
        return deleted;
    }
    */

    async deleteAllSources() {
        try {
            return await this.sourceModel.deleteMany({}); 
        }
        catch(error) {
            console.log(error)
        }

    }

    // update source

    async updateSource(uri: String, updateDto: UpdateDTO) {
        try {
            return await this.sourceModel.updateOne({ uri }, { title: updateDto.title, description: updateDto.description })
        }
        catch(error) {
            console.log(error)
        }
    }

    /*
     async updateSource(uri: String, updateDto: UpdateDTO) {
            const source: Source = await this.getSourceByUri(uri);
        
        if (isNullOrUndefined(source)) {
            console.log('source not found');
            throw new BadRequestException('Source not found');
        }
 
        const updated = await this.sourceModel.updateOne({ uri }, { title: updateDto.title, description: updateDto.description });
 
        if (updated.n === 0 || updated.ok !== 1) {
            console.log('Error Occured');
            throw new InternalServerErrorException('Error Occured while trying to delete');
        }
 
        return updated;
    }
    */

    async updateAllSources(updateDto: UpdateDTO) {
        try {
            return await this.sourceModel.updateMany({}, { title: updateDto.title, description: updateDto.description })
        }
        catch(err) {
            console.log(err);
        }

        // if (updated.n === 0 || updated.ok !== 1) {
        //     console.log('Error Occured');
        //     throw new InternalServerErrorException('Error Occured while trying to update');
        // }
    }
}
